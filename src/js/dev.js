function shopSliderInit() {
    var slider = $('.js-shop-slider');
    if (slider) {
        slider.slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev slider-arrow">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slider-arrow slider-arrow--next">Next</button>',
            responsive: [
                {
                    breakpoint: 1480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
}

function keySliderInit() {
    var slider = $('.js-key-slider');
    if (slider) {
        slider.slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev slider-arrow">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slider-arrow slider-arrow--next">Next</button>',
            responsive: [
                {
                    breakpoint: 1480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
}

function agentSliderInit() {
    var slider = $('.js-agent-slider');
    if (slider) {
        slider.slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev slider-arrow">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slider-arrow slider-arrow--next">Next</button>',
            responsive: [
                {
                    breakpoint: 1480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
}

function changeTab() {
    $(document).on('click', '.js-tab', function (e) {
        e.preventDefault();
        var current_tab = $(this);
        var box = $(this).closest('.tabs').find('.tabs__content');
        // $(this).parent().css('background', 'red');
        // var bodies_box = $(this).parents('.tabs').children('.tabs__bodies');
        var current_index = $(this).index();

        if (current_tab.hasClass('active')) {
            return;
        } else {
            $('.tabs__head').removeClass('active');
            current_tab.addClass('active');
            box.removeClass('active');
            box.eq(current_index).addClass('active');
        }
        // console.log(current_index);
    })
}

$(document).ready(function () {
    console.log('asdasdasd');
   shopSliderInit();
   keySliderInit();
    agentSliderInit();
    changeTab();

});